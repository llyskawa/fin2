package pl.lyskawa.data.user.dailymotion

import com.google.gson.annotations.SerializedName
import pl.lyskawa.domain.user.UserDataSource
import pl.lyskawa.domain.user.UserEntity

data class DailymotionUserApiModel(
    val username: String,
    @SerializedName("avatar_360_url") val avatarUrl: String
) {
    fun toEntity() = UserEntity(username, avatarUrl, UserDataSource.DAILYMOTION)
}