package pl.lyskawa.data.user.github

import io.reactivex.Single
import retrofit2.http.GET

interface GithubService{
        @GET("users")
        fun fetchUsers(): Single<List<GithubUserApiModel>>
}