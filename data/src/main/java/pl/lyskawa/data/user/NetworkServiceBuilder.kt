package pl.lyskawa.data.user

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import pl.lyskawa.data.BuildConfig
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object NetworkServiceBuilder {

    inline fun <reified T> create(
        baseUrl: String,
        client: OkHttpClient = buildClient(),
        converterFactory: Converter.Factory = buildGsonConverterFactory(),
        callAdapter: CallAdapter.Factory = buildCallAdapterFactory()
    ): T =
        Retrofit.Builder()
            .client(client)
            .baseUrl(baseUrl)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(callAdapter)
            .build()
            .create(T::class.java)

    fun buildCallAdapterFactory(): RxJava2CallAdapterFactory =
        RxJava2CallAdapterFactory.create()

    fun buildGsonConverterFactory(): GsonConverterFactory =
        GsonBuilder()
            .serializeNulls()
            .create()
            .let(GsonConverterFactory::create)

    fun buildClient(): OkHttpClient =
        OkHttpClient.Builder()
            .apply { addDebugLoggingInterceptor() }
            .build()

    private fun OkHttpClient.Builder.addDebugLoggingInterceptor() {
        if (!BuildConfig.DEBUG) return
        addInterceptor(
            HttpLoggingInterceptor().apply {
                level = Level.BODY
            }
        )
    }

}

