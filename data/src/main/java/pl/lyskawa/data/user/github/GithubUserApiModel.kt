package pl.lyskawa.data.user.github

import com.google.gson.annotations.SerializedName
import pl.lyskawa.domain.user.UserDataSource
import pl.lyskawa.domain.user.UserEntity

data class GithubUserApiModel(
    @SerializedName("login") val username: String,
    @SerializedName("avatar_url") val avatarUrl: String
) {
    fun toEntity() = UserEntity(username, avatarUrl, UserDataSource.GITHUB)
}