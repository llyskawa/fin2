package pl.lyskawa.data.user.dailymotion

import io.reactivex.Single
import pl.lyskawa.data.user.dailymotion.DailymotionUserApiModel
import retrofit2.http.GET

interface DailymotionService{
    @GET("users?fields=avatar_360_url,username")
    fun fetchUsers(): Single<DailyMotionApiResponse>
}