package pl.lyskawa.data.user

import io.reactivex.Flowable
import pl.lyskawa.data.user.dailymotion.DailymotionService
import pl.lyskawa.data.user.dailymotion.DailymotionUserApiModel
import pl.lyskawa.data.user.github.GithubService
import pl.lyskawa.data.user.github.GithubUserApiModel
import pl.lyskawa.domain.Result
import pl.lyskawa.domain.toResult
import pl.lyskawa.domain.user.UserEntity
import pl.lyskawa.domain.user.UserRepository
import javax.inject.Inject

class UserRepositoryImpl
@Inject
constructor(
    private val githubService: GithubService,
    private val dailymotionService: DailymotionService
) : UserRepository {

    override fun getUserList(): Flowable<Result<List<UserEntity>>> =
        githubService.fetchUsers()
            .map { it.map(GithubUserApiModel::toEntity) }
            .mergeWith(
                dailymotionService.fetchUsers()
                    .map { it.list.map(DailymotionUserApiModel::toEntity) })
            .toResult()
}