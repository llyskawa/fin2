package pl.lyskawa.data.user.dailymotion

data class DailyMotionApiResponse(val list: List<DailymotionUserApiModel>)