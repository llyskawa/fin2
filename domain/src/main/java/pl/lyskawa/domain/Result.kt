package pl.lyskawa.domain

import io.reactivex.Flowable

sealed class Result<T> {

    class InProgress<T> : Result<T>()
    data class Success<T>(val data: T) : Result<T>()
    data class Error<T>(val throwable: Throwable) : Result<T>()

    companion object {
        fun <T> inProgress(): Result<T> = InProgress()
        fun <T> success(data: T): Result<T> = Success(data)
        fun <T> error(throwable: Throwable): Result<T> = Error(throwable)
    }
}

fun <T> Flowable<T>.toResult(): Flowable<Result<T>> = this
    .map { Result.success(it!!) }
    .onErrorReturn { Result.error(it) }
    .startWith(Result.inProgress())