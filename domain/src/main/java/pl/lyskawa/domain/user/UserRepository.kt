package pl.lyskawa.domain.user

import io.reactivex.Flowable
import pl.lyskawa.domain.Result

interface UserRepository{
    fun getUserList(): Flowable<Result<List<UserEntity>>>
}