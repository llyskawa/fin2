package pl.lyskawa.domain.user

import io.reactivex.schedulers.Schedulers
import pl.lyskawa.domain.user.list.UserListNavigator
import javax.inject.Inject

class UserListInteractor
@Inject
constructor(private val navigator: UserListNavigator, private val repository: UserRepository) {

    fun showDetails(userEntity: UserEntity) =
        navigator.showDetails(userEntity)

    fun getUsers() =
        repository.getUserList().subscribeOn(Schedulers.io())
}