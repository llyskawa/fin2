package pl.lyskawa.domain.user

data class UserEntity (val username:String, val avatarUrl: String, val source: UserDataSource)