package pl.lyskawa.domain.user

enum class UserDataSource {
    DAILYMOTION,
    GITHUB
}