package pl.lyskawa.domain.user.list

import pl.lyskawa.domain.navigator.Navigator
import pl.lyskawa.domain.user.UserEntity

interface UserListNavigator : Navigator {
    fun showDetails(user: UserEntity)
}