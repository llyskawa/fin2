package pl.lyskawa.domain.navigator

import androidx.appcompat.app.AppCompatActivity
import java.lang.ref.WeakReference

interface CurrentActivityProvider{
    val currentActivity: WeakReference<AppCompatActivity>?
}