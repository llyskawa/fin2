package pl.lyskawa.domain.navigator

import androidx.navigation.NavDirections

interface Navigator {
    fun navigateUp()
    fun navigateByAction(action: NavDirections)
}