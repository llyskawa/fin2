package pl.lyskawa.finusers.binding

import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("netPath")
fun bindNetworkPhoto(imageView: ImageView, path: String?) {
    Glide.with(imageView.context)
        .load(path?.let(Uri::parse))
        .into(imageView)
}