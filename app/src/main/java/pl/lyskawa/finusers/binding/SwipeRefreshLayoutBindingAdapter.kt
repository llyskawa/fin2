package pl.lyskawa.finusers.binding

import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

@BindingAdapter("isRefreshing")
fun bindIsRefreshing(view: SwipeRefreshLayout, isRefreshing: Boolean) {
    view.isRefreshing = isRefreshing
}

@BindingAdapter("onRefresh")
fun bindOnRefresh(view: SwipeRefreshLayout, onRefresh: Runnable) {
    view.setOnRefreshListener { onRefresh.run() }
}