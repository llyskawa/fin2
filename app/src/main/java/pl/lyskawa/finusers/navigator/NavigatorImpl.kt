package pl.lyskawa.finusers.navigator

import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import pl.lyskawa.domain.navigator.CurrentActivityProvider
import pl.lyskawa.domain.navigator.Navigator
import pl.lyskawa.finusers.R
import javax.inject.Inject

open class NavigatorImpl
@Inject
constructor(private val currentActivityProvider: CurrentActivityProvider) : Navigator {

    override fun navigateUp() {
        synchronized(this) {
            currentActivityProvider.currentActivity
                ?.get()
                ?.let { activity ->
                    val success = activity.navController().navigateUp()
                    if (!success) {
                        activity.onBackPressed()
                    }
                }
        }
    }

    override fun navigateByAction(action: NavDirections) {
        synchronized(this) {
            currentActivityProvider.currentActivity
                ?.get()
                ?.navController()
                ?.navigate(action)
        }
    }

    private fun AppCompatActivity.navController(): NavController =
        Navigation.findNavController(this, R.id.container)

}