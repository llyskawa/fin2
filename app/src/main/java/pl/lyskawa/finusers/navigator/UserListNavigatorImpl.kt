package pl.lyskawa.finusers.navigator

import android.content.res.Resources
import pl.lyskawa.domain.navigator.CurrentActivityProvider
import pl.lyskawa.domain.user.UserEntity
import pl.lyskawa.domain.user.list.UserListNavigator
import pl.lyskawa.finusers.ui.userdetails.UserDetailsUIModel
import pl.lyskawa.finusers.ui.userlist.UserListFragmentDirections
import javax.inject.Inject

class UserListNavigatorImpl
@Inject
constructor(
    currentActivityProvider: CurrentActivityProvider,
    private val resources: Resources
) : NavigatorImpl(currentActivityProvider), UserListNavigator {

    override fun showDetails(user: UserEntity) {
        navigateByAction(
            UserListFragmentDirections.actionUserListFragmentToUserDetailsActivity(
                UserDetailsUIModel.fromEntity(user, resources)
            )
        )
    }

}