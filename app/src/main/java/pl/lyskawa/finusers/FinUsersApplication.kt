package pl.lyskawa.finusers

import android.app.Activity
import android.app.Application
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import pl.lyskawa.finusers.di.AppComponent
import pl.lyskawa.finusers.di.AppInjector
import pl.lyskawa.finusers.lifecycle.CurrentActivityProviderImpl
import timber.log.Timber
import javax.inject.Inject

class FinUsersApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    private lateinit var appComponent: AppComponent

    override fun activityInjector() = dispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        appComponent = AppInjector.init(
            this,
            CurrentActivityProviderImpl(this)
        )
        enableTimber()
    }

    private fun enableTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}