package pl.lyskawa.finusers.lifecycle

import android.app.Activity
import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import pl.lyskawa.domain.navigator.CurrentActivityProvider
import java.lang.ref.WeakReference

class CurrentActivityProviderImpl(application: Application) : CurrentActivityProvider {

    override val currentActivity: WeakReference<AppCompatActivity>?
        get() = reference

    private var reference: WeakReference<AppCompatActivity>? = null

    init {
        application.registerActivityLifecycleCallbacks(
            ActivityLifecycleCallback(
                onCreated = this::reset,
                onStarted = this::reset,
                onResumed = this::reset,
                onStopped = this::threadSafeClear
            )
        )
    }

    private fun threadSafeClear(activity: Activity) {
        synchronized(this) {
            clear(activity)
        }
    }

    private fun reset(activity: Activity) {
        synchronized(this) {
            clear(activity)
            val appCompatActivity = activity as AppCompatActivity
            reference = WeakReference(appCompatActivity)
        }
    }

    private fun clear(activity: Activity) {
        if (activity == reference?.get()) {
            reference?.clear()
            reference = null
        }
    }
}