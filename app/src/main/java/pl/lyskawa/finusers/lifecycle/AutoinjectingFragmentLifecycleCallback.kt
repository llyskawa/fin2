package pl.lyskawa.finusers.lifecycle

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import dagger.android.support.AndroidSupportInjection
import pl.lyskawa.finusers.di.Injectable

object AutoinjectingFragmentLifecycleCallback : FragmentManager.FragmentLifecycleCallbacks() {
    override fun onFragmentPreCreated(fm: FragmentManager, f: Fragment, savedInstanceState: Bundle?) {
        if (f is Injectable) {
            AndroidSupportInjection.inject(f)
        }
    }
}