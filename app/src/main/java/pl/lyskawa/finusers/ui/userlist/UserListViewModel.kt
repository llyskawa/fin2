package pl.lyskawa.finusers.ui.userlist

import android.content.res.Resources
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableList
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import pl.lyskawa.domain.Result
import pl.lyskawa.domain.user.UserEntity
import pl.lyskawa.domain.user.UserListInteractor
import pl.lyskawa.finusers.ui.userdetails.UserDetailsUIModel
import timber.log.Timber
import javax.inject.Inject

class UserListViewModel
@Inject
constructor(private val interactor: UserListInteractor, private val resources: Resources) : ViewModel() {

    val userList: ObservableList<UserDetailsUIModel> = ObservableArrayList<UserDetailsUIModel>()
    val isLoading = ObservableBoolean(false)

    val onUserClicked = { user: UserDetailsUIModel ->
        interactor.showDetails(user.toEntity())
    }

    private val compositeDisposable = CompositeDisposable()

    init {
        refresh()
    }

    fun refresh() {
        userList.clear()
        interactor.getUsers()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::handleFetchingResult, this::handleFetchingError)
            .addTo(compositeDisposable)
    }

    private fun handleFetchingResult(result: Result<List<UserEntity>>) {
        when (result) {
            is Result.InProgress -> isLoading.set(true)
            else -> {
                isLoading.set(false)
                when (result) {
                    is Result.Error -> handleFetchingError(result.throwable)
                    is Result.Success ->
                        userList.addAll(result.data.map { UserDetailsUIModel.fromEntity(it, resources) })
                }
            }

        }
    }

    private fun handleFetchingError(throwable: Throwable) {
        Timber.e(throwable)
        //TODO show error on UI? snackbar/toast?
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}