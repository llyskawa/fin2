package pl.lyskawa.finusers.ui.userdetails

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class UserDetailsViewModel
@Inject
constructor() : ViewModel() {
    val username = ObservableField<String>()
    val avatarUrl = ObservableField<String>()
    val source = ObservableField<String>()

    fun init(user: UserDetailsUIModel) {
        username.set(user.username)
        avatarUrl.set(user.avatarUrl)
        source.set(user.sourceString)
    }
}