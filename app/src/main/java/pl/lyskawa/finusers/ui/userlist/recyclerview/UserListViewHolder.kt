package pl.lyskawa.finusers.ui.userlist.recyclerview

import androidx.recyclerview.widget.RecyclerView
import pl.lyskawa.domain.user.UserEntity
import pl.lyskawa.finusers.databinding.ItemUserDetailsBinding
import pl.lyskawa.finusers.ui.userdetails.UserDetailsUIModel

class UserListViewHolder(private val binding: ItemUserDetailsBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(userModel: UserDetailsUIModel) {
        binding.viewModel?.bind(userModel)
        binding.executePendingBindings()
    }

}