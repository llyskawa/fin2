package pl.lyskawa.finusers.ui.userdetails

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.navArgs
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import pl.lyskawa.finusers.R
import pl.lyskawa.finusers.databinding.ActivityUserDetailsBinding
import pl.lyskawa.finusers.di.Injectable
import javax.inject.Inject

class UserDetailsActivity : AppCompatActivity(), HasSupportFragmentInjector, Injectable {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: UserDetailsViewModel
    lateinit var binding: ActivityUserDetailsBinding

    val args: UserDetailsActivityArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_details)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserDetailsViewModel::class.java)

        binding.viewModel = viewModel
        viewModel.init(args.user)

        val savedStateUser = savedInstanceState?.getParcelable<UserDetailsUIModel>(SELECTED_USER)
        if (savedStateUser != null) {
            viewModel.init(savedStateUser)
        } else {
            viewModel.init(args.user)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(SELECTED_USER, args.user)
        super.onSaveInstanceState(outState)
    }

    companion object {
        private const val SELECTED_USER = "SELECTED_USER"
    }
}