package pl.lyskawa.finusers.ui.userdetails

import android.content.res.Resources
import android.os.Parcel
import android.os.Parcelable
import pl.lyskawa.domain.user.UserDataSource
import pl.lyskawa.domain.user.UserEntity
import pl.lyskawa.finusers.R

data class UserDetailsUIModel(
    val username: String,
    val avatarUrl: String,
    val source: UserDataSource,
    val sourceString: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        UserDataSource.values()[parcel.readInt()],
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(username)
        parcel.writeString(avatarUrl)
        parcel.writeInt(source.ordinal)
        parcel.writeString(sourceString)
    }

    override fun describeContents(): Int {
        return 0
    }

    fun toEntity() =
        UserEntity(username, avatarUrl, source)

    companion object CREATOR : Parcelable.Creator<UserDetailsUIModel> {
        override fun createFromParcel(parcel: Parcel): UserDetailsUIModel {
            return UserDetailsUIModel(parcel)
        }

        override fun newArray(size: Int): Array<UserDetailsUIModel?> {
            return arrayOfNulls(size)
        }

        fun fromEntity(entity: UserEntity, resources: Resources): UserDetailsUIModel {
            val sourceString = resources.getString(
                when (entity.source) {
                    UserDataSource.DAILYMOTION -> R.string.dailymotion_source
                    UserDataSource.GITHUB -> R.string.github_source
                }
            )

            return UserDetailsUIModel(entity.username, entity.avatarUrl, entity.source, sourceString)
        }

    }


}