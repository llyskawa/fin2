package pl.lyskawa.finusers.ui.userlist.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.lyskawa.finusers.databinding.ItemUserDetailsBinding
import pl.lyskawa.finusers.ui.userdetails.UserDetailsUIModel

class UserListAdapter : RecyclerView.Adapter<UserListViewHolder>() {

    var items: List<UserDetailsUIModel> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var onItemClick: (UserDetailsUIModel) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemUserDetailsBinding.inflate(inflater, parent, false)
        binding.viewModel = UserListItemViewModel(onItemClick)
        return UserListViewHolder(binding)
    }

    override fun getItemCount(): Int = items.count()

    override fun onBindViewHolder(holder: UserListViewHolder, position: Int) =
        holder.bind(items[position])

}

