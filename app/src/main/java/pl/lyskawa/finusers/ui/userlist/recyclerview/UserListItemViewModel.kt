package pl.lyskawa.finusers.ui.userlist.recyclerview

import androidx.databinding.ObservableField
import pl.lyskawa.finusers.ui.userdetails.UserDetailsUIModel

class UserListItemViewModel(val onItemClick: (UserDetailsUIModel) -> Unit) {
    val username = ObservableField<String>()
    val avatarUrl = ObservableField<String>()
    val source = ObservableField<String>()

    lateinit var model: UserDetailsUIModel

    fun bind(item: UserDetailsUIModel) {
        model = item
        username.set(model.username)
        avatarUrl.set(model.avatarUrl)
        source.set(model.sourceString)
    }

    fun onClick() {
        onItemClick(model)
    }
}