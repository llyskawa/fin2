package pl.lyskawa.finusers.ui.userlist.recyclerview

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.lyskawa.finusers.ui.userdetails.UserDetailsUIModel

class UserListRecyclerView
@JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : RecyclerView(context, attrs, defStyle) {
    private val adapter = UserListAdapter()

    init {
        setAdapter(adapter)
        layoutManager = GridLayoutManager(context, 2)
    }

    fun setItems(users: List<UserDetailsUIModel>) {
        adapter.items = users
    }

    fun setOnItemClick(callback: (UserDetailsUIModel) -> Unit) {
        adapter.onItemClick = callback
    }
}