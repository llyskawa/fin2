package pl.lyskawa.finusers.di

import android.app.Application
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import pl.lyskawa.domain.navigator.CurrentActivityProvider
import pl.lyskawa.domain.navigator.Navigator
import pl.lyskawa.domain.user.list.UserListNavigator
import pl.lyskawa.finusers.di.viewmodel.ViewModelModule
import pl.lyskawa.finusers.navigator.NavigatorImpl
import pl.lyskawa.finusers.navigator.UserListNavigatorImpl
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideNavigator(currentActivityProvider: CurrentActivityProvider): Navigator =
        NavigatorImpl(currentActivityProvider)

    @Provides
    @Singleton
    fun provideUserListNavigator(
        currentActivityProvider: CurrentActivityProvider,
        resources: Resources
    ): UserListNavigator =
        UserListNavigatorImpl(currentActivityProvider, resources)

    @Provides
    @Singleton
    fun provideResources(application: Application): Resources = application.resources
}