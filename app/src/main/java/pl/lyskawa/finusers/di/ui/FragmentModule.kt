package pl.lyskawa.finusers.di.ui

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.lyskawa.finusers.ui.userlist.UserListFragment

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeUserListFragment(): UserListFragment
}