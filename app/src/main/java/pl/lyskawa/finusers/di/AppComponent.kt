package pl.lyskawa.finusers.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import pl.lyskawa.domain.navigator.CurrentActivityProvider
import pl.lyskawa.finusers.FinUsersApplication
import pl.lyskawa.finusers.di.ui.ActivityModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        DataModule::class,
        ActivityModule::class,
        DomainModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun activityProvider(activityProvider: CurrentActivityProvider): Builder

        fun build(): AppComponent
    }

    fun inject(application: FinUsersApplication)
}