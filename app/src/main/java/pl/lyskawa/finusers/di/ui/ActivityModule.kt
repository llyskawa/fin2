package pl.lyskawa.finusers.di.ui

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.lyskawa.finusers.ui.userdetails.UserDetailsActivity
import pl.lyskawa.finusers.ui.userlist.UserListActivity

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeUserListActivity(): UserListActivity

    @ContributesAndroidInjector
    abstract fun contributeUserDetailsActivity(): UserDetailsActivity
}