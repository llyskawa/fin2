package pl.lyskawa.finusers.di

import android.app.Activity
import androidx.fragment.app.FragmentActivity
import dagger.android.AndroidInjection
import dagger.android.support.HasSupportFragmentInjector
import pl.lyskawa.domain.navigator.CurrentActivityProvider
import pl.lyskawa.finusers.FinUsersApplication
import pl.lyskawa.finusers.lifecycle.ActivityLifecycleCallback
import pl.lyskawa.finusers.lifecycle.AutoinjectingFragmentLifecycleCallback

object AppInjector {
    fun init(
        application: FinUsersApplication,
        activityProvider: CurrentActivityProvider
    ): AppComponent {
        val component = DaggerAppComponent.builder()
            .application(application)
            .activityProvider(activityProvider)
            .build()

        component.inject(application)

        application.registerActivityLifecycleCallbacks(
            ActivityLifecycleCallback(onCreated = this::handleActivity)
        )

        return component
    }

    private fun handleActivity(activity: Activity) {
        if (activity is HasSupportFragmentInjector) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager.registerFragmentLifecycleCallbacks(
                AutoinjectingFragmentLifecycleCallback, true
            )
        }
    }
}