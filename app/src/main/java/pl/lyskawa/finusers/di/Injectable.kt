package pl.lyskawa.finusers.di

/**
 * Use it to mark activities & fragments as injectable
 */
interface Injectable