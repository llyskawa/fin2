package pl.lyskawa.finusers.di

import dagger.Module
import dagger.Provides
import pl.lyskawa.data.user.NetworkServiceBuilder
import pl.lyskawa.data.user.UserRepositoryImpl
import pl.lyskawa.data.user.dailymotion.DailymotionService
import pl.lyskawa.data.user.github.GithubService
import pl.lyskawa.domain.user.UserRepository
import pl.lyskawa.finusers.BuildConfig
import pl.lyskawa.finusers.di.viewmodel.ViewModelModule
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class DataModule {

    @Provides
    @Singleton
    fun provideGithubService(): GithubService =
        NetworkServiceBuilder.create(BuildConfig.BACKEND_GITHUB_URL)

    @Provides
    @Singleton
    fun provideDailymotionService(): DailymotionService =
        NetworkServiceBuilder.create(BuildConfig.BACKEND_DAILYMOTION_URL)

    @Provides
    @Singleton
    fun provideUserRepository(githubService: GithubService, dailymotionService: DailymotionService): UserRepository =
        UserRepositoryImpl(githubService, dailymotionService)
}