package pl.lyskawa.finusers.di

import dagger.Module
import pl.lyskawa.finusers.di.viewmodel.ViewModelModule

@Module(includes = [ViewModelModule::class])
class DomainModule {

}